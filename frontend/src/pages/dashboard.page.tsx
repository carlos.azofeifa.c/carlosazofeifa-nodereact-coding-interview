import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress, Snackbar, Autocomplete, TextField } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

const SEARCH_OPTIONS = [
  { label: "Ttitle", id: 1 },
  { label: "Gender", id: 2 },
  { label: "Company", id: 2 }
]

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [isError, setIsError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [filters, setFilters] = useState({})

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await backendClient.getAllUsers();
        setUsers(result.data);
      } catch (err) {
        setIsError(true)
      } finally {
        setLoading(false)
      }
    };
    fetchData();
  }, []);

  const handleChange = (e: React.SyntheticEvent, value: { label: string, id: number } | null) => {
    console.info(value)
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <Snackbar
        open={isError}
        autoHideDuration={6000}
        onClose={() => setIsError(false)}
        message="Something went wrong. Please try again later"
      />
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "95vh",
              margin: "auto"
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <div>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={SEARCH_OPTIONS}
              sx={{ width: 300 }}
              onChange={(event: React.SyntheticEvent, newValue: { label: string, id: number } | null) => {
                handleChange(event, newValue)
              }}
              renderInput={(params) => <TextField {...params} label="Search..." />}
            />
            </div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
